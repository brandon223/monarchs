<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Home Page
Route::get('/', function () {
    return view('index');
});

// Shop Index & Individual Product Page
Route::get('/shop', 'ShopController@getIndex');
Route::get('/shop/{product_id}', 'ShopController@getProduct');

// Cart
Route::get('/cart', 'ShopController@getCart');
Route::post('/cart/add', 'ShopController@postAddToCart');
Route::get('/cart/remove/{row_id}', 'ShopController@getRemoveFromCart');

// Login
Route::get('/signin', function () {
	return view('auth.signin');
});
Route::post('/signin/auth', 'AuthController@postSignIn');

// Registration
Route::get('/signup', function () {
	return view('auth.signup');
});
Route::post('/signup/submit', 'AuthController@postSignUp');

// Logout
Route::get('/signout', function () {
	if(Auth::check()) {
		Auth::Logout();
		return Redirect::to('')->with('message', '<div class="alert alert-success">You have been logged out, have a nice day!</div>');
	}
	else{
		return Redirect::to('')->with('message', '<div class="alert alert-danger">Failed to logout, you are not signed in to an account.</div>');
	}
});

// Administrative Control Dash
Route::get('/admin', 'AdminController@getIndex');
Route::post('/admin/product/submit', 'ShopController@postCreateProduct');
//End Routes
