<?php

namespace App\Http\Controllers;

use Mail;
use Hash;
use Auth;
use App\User;
use Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    public function postSignUp(Request $request)
    {
        $data = $request->all();
        $user_email = $data['email'];
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3'
        );

        $validator = Validator::make($data, $rules);
        if($validator->passes()) {
            // Validation Passed
            try {
                // Declare new instance of the "User" model
                $User = new User();

                // Insert user account data into the DB
                $User->email = $data['email'];
                $User->password = Hash::make($data['password']);
                $User->ip = $_SERVER['REMOTE_ADDR'];
                $User->save();

                // Once user account information has been inserted, a confirmation email is sent to the email.
                Mail::send('emails.registration', $data, function($message) use($user_email)
                {
                    $message->to($user_email, 'Valued Customer')->subject('Welcome to the Anderson Monarchs Store');
                });

                // Redirect User to the Sign In Page with a return message/bootstrap alert.
                return Redirect::to('/signin')->with('message', '<div class="alert alert-success">Registration complete! Just login to use your account :-)');
            }
            catch(Exception $e) {
                return Redirect::to('')->with('message', '<div class="alert alert-danger">Error: ' . $e . '</div>');
            }
        }
        else{
            // Validation Failed
            return Redirect::to('/signup')->with('message', '<div class="alert alert-danger">Error: Please make sure you have filled out all fields.</div>');
        }
    }

    public function postSignIn(Request $request)
    {
        $data = $request->all();
        $rules = array(
            'email' => 'Required',
            'password' => 'Required'
        );

        $validator = Validator::make($data, $rules);
        if($validator->passes()) {
            // Validation Passed
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']]))
            {
                // The user is being remembered...
                return Redirect::to('')->with('message', '<div class="alert alert-success">Successfully signed in.</div>');
            }
            else{
                return Redirect::to('/signin')->with('message', '<div class="alert alert-danger">Error: Failed to authenticate with the provided credentials.</div>');
            }
        }
        else {
            // Validation Failed
            return Redirect::to('/signin')->with('message', '<div class="alert alert-danger">Error: Please make sure you have filled out all fields.</div>');
        }
    }
}
