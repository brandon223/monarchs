<?php

namespace App\Http\Controllers;

use Mail;
use Hash;
use Auth;
use App\User;
use App\Product;
use Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AdminController extends Controller
{
    public function getIndex()
    {
        if(Auth::check()) {
            if(Auth::user()->is_admin == "true") {
                $users = User::orderBy('id', 'DSC')->get();
                $products =  Product::orderBy('id', 'DSC')->get();
                return view('admin.index', ['users' => $users, 'products' => $products])->with('message', '<div class="alert alert-success">Welcome back, Administrator.</div>');
            }
            else {
                //Not an Admin
                return Redirect::to('')->with('message', '<div class="alert alert-danger">Access Denied.</div>');
            }
        }
        else {
            //Not an Admin
            return Redirect::to('')->with('message', '<div class="alert alert-danger">Access Denied.</div>');
        }
    }
}
