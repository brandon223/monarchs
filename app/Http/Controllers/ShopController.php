<?php

namespace App\Http\Controllers;

use Mail;
use Hash;
use Auth;
use Cart;
use App\Product;
use Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ShopController extends Controller
{
    private $base_url;

    public function __construct()
    {
      $this->base_url = "http://monarchs:8888";
    }

    public function getIndex()
    {
        $products =  Product::orderBy('id', 'DSC')->get();
        return view('shop.index', ['products' => $products]);
    }

    public function postCreateProduct(Request $request)
    {
        if(Auth::check()) {
            if(Auth::user()->is_admin == "true") {
                $data = $request->all();
                $rules = array(
                    'title' => 'Required',
                    'category' => 'Required',
                    'description' => 'Required',
                    'picture' => 'Required',
                    'price' => 'Required'
                );

                $validator = Validator::make($data, $rules);
                if($validator->passes()) {
                    try{
                        $pid = uniqid('monarchs_')  . '.jpg';
                        $picture_url = $this->base_url . '/product_images/' . $pid;
                        $request->file('picture')->move(base_path() . '/public/product_images/', $pid);

                        $Product = new Product();
                        $Product->title = $data['title'];
                        $Product->category = $data['category'];
                        $Product->description = $data['description'];
                        $Product->picture_url = $picture_url;
                        $Product->price = $data['price'];

                        $Product->save();

                        return Redirect::to('')->with('message', '<div class="alert alert-success">Product has been uploaded to store!</div>');
                    }
                    Catch(Exception $e) {
                       return Redirect::to('')->with('message', '<div class="alert alert-danger">Error: ' . $e . '</div>');
                    }
                }
                else{
                    return Redirect::to('')->with('message', '<div class="alert alert-danger">Error: Please make sure all fields are completed.</div>');
                }
            }
            else{
                return Redirect::to('')->with('message', '<div class="alert alert-danger">Access Denied.</div>');
            }
        }
        else{
            return Redirect::to('')->with('message', '<div class="alert alert-danger">Access Denied.</div>');
        }
    }

    public function getProduct($product_id)
    {
        $product =  Product::find($product_id);
        return view('shop.product', ['product' => $product]);
    }

    public function postAddToCart(Request $request)
    {
      /**
       * Add a row to the cart
       *
       * @param string|Array $id      Unique ID of the item|Item formated as array|Array of items
       * @param string       $name    Name of the item
       * @param int          $qty     Item qty to add to the cart
       * @param float        $price   Price of one item
       * @param Array        $options Array of additional options, such as 'size' or 'color'
      */
      $data = $request->all();
      $rules = array(
        'quantity' => 'Required',
        'size' => 'Required'
      );
      $validator = Validator::make($data, $rules);
      if($validator->passes()) {
        //validation Passed
        try {
          $product = Product::find($data['product_id']);

          Cart::add($product->id, $product->title, $data['quantity'], $product->price, array('size' => $data['size']));

          return Redirect::to('cart');
        } catch (Exception $e) {
          return $e->getMessage();
        }
      }
      else{
        //validation Failed
      }
    }

    public function getRemoveFromCart($row_id)
    {
      try {
        Cart::remove($row_id);
        return Redirect::to('cart');
      } catch (Exception $e) {
        return $e->getMessage();
      }
    }

    public function getCart()
    {
      $cart = Cart::content();
      return view('shop.cart', ['cart' => $cart]);
    }
}
