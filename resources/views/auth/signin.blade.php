@include('header')
<div class="container">
  <form class="form-signin" method="POST" action="{{ URL::to('signin/auth') }}">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <h2 class="form-signin-heading"><center>Sign In Form</center></h2>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
    <div class="checkbox">
      <label>
        <input type="checkbox" name="remember" value=""> Remember me
      </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <a href="{!! URL::to('signup') !!}">Don't have an account? Sign Up!</a>
  </form>
</div>
@include('footer')