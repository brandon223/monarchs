@include('header')
<div class="container">
  <form class="form-signin" method="POST" action="{{ URL::to('signup/submit') }}">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <h2 class="form-signin-heading"><center>Sign Up Form</center></h2>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="true" autofocus="">
    <label for="Password" class="sr-only">Password</label>
    <input type="password" name="password" id="Password" class="form-control" placeholder="Password" required="true" style="margin-bottom: 0px;border-bottom: 0;border-bottom-left-radius: 0;border-bottom-right-radius: 0;">
    <label for="PasswordConfirm" class="sr-only">Password Confirmation</label>
    <input type="password" name="password_confirmation" id="PasswordConfirm" class="form-control" placeholder="Password Confirmation" required="true">
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
    <a href="{!! URL::to('signin') !!}">Already have an account? Sign In!</a>
  </form>
</div>
@include('footer')