<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Anderson Monarchs Store</title>
        <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/signin.css') }}" />
        <!--<link rel="stylesheet" type="text/css" href="css/shop-item.css" />-->
        <script src="{{ URL::to('http://code.jquery.com/jquery-1.11.3.min.js') }}"></script>
        <script src="{{ URL::to('js/holder.js') }}"></script>
        <script src="{{ URL::to('js/bootstrap.js') }}"></script>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-inverse" style="border-radius: 0;">
            <div class="container">
                <a class="navbar-brand" href="{{ URL::to('') }}">Anderson Monarchs</a>
                <ul class="nav navbar-nav" style="float: right;">
                  <li class="nav-item active">
                    <a class="nav-link" href="{{ URL::to('') }}">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('shop') }}">Shop</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ URL::to('cart') }}">Cart ({{ Cart::count()  }})</a>
                  </li>
                  @if(Auth::check())
                    @if(Auth::user()->is_admin == "true")
                      <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin') }}">Admin CP</a>
                      </li>
                    @else
                    @endif
                    <li class="nav-item">
                      <a class="nav-link" href="{{ URL::to('signout') }}">Logout, {!! Auth::user()->email !!}</a>
                    </li>
                  @else
                    <li class="nav-item">
                      <a class="nav-link" href="{{ URL::to('signin') }}">Login/Register</a>
                    </li>
                  @endif
                </ul>
            </div>
        </nav>
        <div class="container">
          <div class="return-alert" style="margin-top: 10px;">
            @if ($message = Session::get('message')) {!! $message !!} @else @endif
          </div>
        </div>
