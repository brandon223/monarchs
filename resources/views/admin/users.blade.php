<table class="table">
  <thead class="thead-inverse">
    <tr>
      <th>ID #</th>
      <th>Email</th>
      <th>IP</th>
      <th>Manage</th>
    </tr>
  </thead>
  <tbody>
    @foreach($users as $user)
      <tr>
        <th scope="row">{!! $user->id !!}</th>
        <td>{!! $user->email !!}</td>
        <td>{!! $user->ip !!}</td>
        <?php $ban_url = 'admin/users/' . $user->id . '/ban'; ?>
        <td><a href="{!! URL::to($ban_url) !!}" class="btn btn-danger">Ban User</a></td>
      </tr>
    @endforeach
  </tbody>
</table>
