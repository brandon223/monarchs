<center><h1>Create A New Product</h1></center>
<form method="POST" action="{!! URL::to('admin/product/submit') !!}" enctype="multipart/form-data" files="true">
<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	<div class="form-group">
		<input type="text" name="title" class="form-control" placeholder="Product Title" />
	</div>

	<div class="form-group">
		<input type="text" name="category" class="form-control" placeholder="Product Category" />
	</div>

	<div class="form-group">
		<input type="text" name="price" class="form-control"  placeholder="Price" />
	</div>

	<div class="form-group">
		<textarea class="form-control" name="description" rows="5" placeholder="Product Description"></textarea>
	</div>

	<fieldset class="form-group">
		<label for="exampleInputFile">Product Picture</label>
		<input type="file" name="picture" class="form-control-file" id="exampleInputFile">
	</fieldset>

	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-primary" value="Submit" />
	</div>
</form>