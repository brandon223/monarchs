<table class="table">
  <thead class="thead-inverse">
    <tr>
      <th>ID #</th>
      <th>Title</th>
      <th>Description</th>
      <th>Category</th>
      <th>Price</th>
      <th>Picture</th>
      <th>Quantity</th>
    </tr>
  </thead>
  <tbody>
  @foreach($products as $product)
    <tr>
      <th scope="row">{!! $product->id !!}</th>
      <td>{!! $product->title !!}</td>
      <td>{!! $product->description !!}</td>
      <td>{!! $product->category !!}</td>
      <td>{!! $product->price !!}</td>
      <th><img src="{!! $product->picture_url !!}" style="width:100px;"></img><a href="#" class="btn btn-info">Change Picture</a></th>
      <th><input type="text" value="{!! $product->quantity !!}" /><input type="submit" class="btn btn-success" value="Update Quantity" />
    </tr>
    @endforeach
  </tbody>
</table>
