@include('header')
  <div class="container">
  	<div class="row">
  		<div class="col-xs-8">
  			<div class="panel panel-info">
  				<div class="panel-heading">
  					<div class="panel-title">
  						<div class="row">
  							<div class="col-xs-6">
  								<h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
  							</div>
  							<div class="col-xs-6">
  								<a href="{!! URL::to('shop') !!}" class="btn btn-primary btn-sm btn-block">
  									<span class="glyphicon glyphicon-share-alt"></span> Continue shopping
  								</a>
  							</div>
  						</div>
  					</div>
  				</div>
          @foreach($cart as $item)
  				<div class="panel-body">
  					<div class="row">
  						<div class="col-xs-2"><img class="img-responsive" src="{!! App\Product::find($item->id)->picture_url !!}">
  						</div>
  						<div class="col-xs-4">
  							<h4 class="product-name"><strong>{!! $item->name !!} [{!! $item->options->size !!}]</strong></h4><h4><small>{!! App\Product::find($item->id)->description !!}</small></h4>
  						</div>
  						<div class="col-xs-6">
  							<div class="col-xs-6 text-right">
  								<h6><strong>{!! $item->price !!}</strong></h6>
  							</div>
  							<div class="col-xs-4">
  								<input type="text" class="form-control input-sm" value="{!! $item->qty !!}">
                  <a href="{!! URL::to('cart/remove/' . $item->rowid) !!}" class="form-control">Remove Item</a>
  							</div>
  							<div class="col-xs-2">
  								<button type="button" class="btn btn-link btn-xs">
  									<span class="glyphicon glyphicon-trash"> </span>
  								</button>
  							</div>
  						</div>
  					</div>
            @endforeach()
  					<hr>
  					<hr>
  				</div>
  				<div class="panel-footer">
  					<div class="row text-center">
  						<div class="col-xs-9">
  							<h4 class="text-right">Total <strong>${!! Cart::total() !!}</strong></h4>
  						</div>
  						<div class="col-xs-3">
  							<button type="button" class="btn btn-success btn-block">
  								Checkout
  							</button>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
@include('footer')
