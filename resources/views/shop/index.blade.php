@include('header')
<div class="container">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
            <p class="lead">Anderson Monarchs</p>
            <div class="list-group">
                <a href="#" class="list-group-item active">Category 1</a>
                <a href="#" class="list-group-item">Category 2</a>
                <a href="#" class="list-group-item">Category 3</a>
            </div>
        </div>
      @foreach($products as $product)
        <div class="col-sm-4">
          <div class="card">
            <img class="card-img-top" src="{!! $product->picture_url !!}" alt="Product Image" style="width:344px; height:212px;">
            <div class="card-block">
              <h4 class="card-title">{!! $product->title !!}</h4>
              <p class="card-text">{!! $product->description !!}</p>
              <a href="{!! URL::to('shop/' . $product->id) !!}" class="btn btn-primary">View More</a>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
</div>
@include('footer')
